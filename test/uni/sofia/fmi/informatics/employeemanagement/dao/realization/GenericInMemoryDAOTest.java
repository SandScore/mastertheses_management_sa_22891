package uni.sofia.fmi.informatics.employeemanagement.dao.realization;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Employee;
import uni.sofia.fmi.informatics.employeemanagement.persistence.inmemorydb.InMemoryDB;

public class GenericInMemoryDAOTest {

	@Mock
	private InMemoryDB dbMock;
	
	private GenericInMemoryDAO<Employee> dao;
	
	@Before
	public void setUp() {
		this.dbMock = Mockito.mock(InMemoryDB.class);
		// set up the methods 
		Mockito.doNothing().when(this.dbMock).add(Mockito.any(Employee.class));
		
		this.dao = new GenericInMemoryDAO<Employee>(dbMock);
	}
	
	@Test
	public void saveTest() {
		Employee employee = new Employee("fn", "ln", "iban", BankType.DSK, 100000);
		
		try {
			this.dao.save(employee);
		} catch (Exception exc) {
			fail();
		}
	}
}
