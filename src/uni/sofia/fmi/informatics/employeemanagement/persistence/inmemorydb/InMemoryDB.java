package uni.sofia.fmi.informatics.employeemanagement.persistence.inmemorydb;

import java.util.HashMap;
import java.util.Map;

import uni.sofia.fmi.informatics.employeemanagement.persistence.Entity;

/**
 * Our in-memory database representation 
 * 
 * @author simo
 *
 */
public class InMemoryDB {
	
	private Map<Class<? extends Entity>, Map<Long, Entity>> dbContainer;
	
	public InMemoryDB() {
		dbContainer = new HashMap<Class<? extends Entity>, Map<Long, Entity>>();
	}
	
	public<T extends Entity> void add(T entity) {
		Map<Long, Entity> entities = obtainEntities(entity);
		
		entities.put(Long.valueOf(entity.getId()), entity);
	}
	
	public<T extends Entity> void update(T entity) {
		this.add(entity);
	}
	
	public<T extends Entity> void remove(T entity) {
		Map<Long, Entity> entities = obtainEntities(entity);
		
		entities.remove(entity.getId());
	}

	@SuppressWarnings("unchecked")
	public<T extends Entity> T findById(Class<? extends Entity> clazz, long id) {
		T searchedEntity = null;
		Map<Long, ? extends Entity> entities = dbContainer.get(clazz);
		if (entities != null) {
			searchedEntity = (T) entities.get(id);
		}
		
		return searchedEntity;
	}

	private <T extends Entity> Map<Long, Entity> obtainEntities(T entity) {
		Map<Long, Entity> entities = dbContainer.get(entity.getClass());
		if (entities == null) {
			entities = new HashMap<Long, Entity>();
			dbContainer.put(entity.getClass(), entities);
		}
		return entities;
	}
}
