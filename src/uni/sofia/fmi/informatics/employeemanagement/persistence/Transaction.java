package uni.sofia.fmi.informatics.employeemanagement.persistence;

import java.util.Date;

import uni.sofia.fmi.informatics.employeemanagement.util.ValidationUtil;

/**
 * Persistence entity represents a Transaction between two bank accounts in a data base 
 * representation
 * 
 * @author simo
 *
 */
public class Transaction extends Entity {
	
	private String receiverIBAN;
	private String senderIBAN;
	private Date transactionDate;
	private double amount;
	private TransactionStatus status;
	
	public Transaction () {
		
	}
	
	public Transaction (String receiver, String sender, Date date, double amount) {
		this.setAmount(amount);
		this.setReceiverIBAN(receiver);
		this.setSenderIBAN(sender);
		this.setTransactionDate(date);
		this.setStatus(TransactionStatus.PENDING);
	}

	public String getReceiverIBAN() {
		return receiverIBAN;
	}

	public void setReceiverIBAN(String receiverIBAN) {
		ValidationUtil.ibanValidation(receiverIBAN);
		
		this.receiverIBAN = receiverIBAN;
	}

	public String getSenderIBAN() {
		return senderIBAN;
	}

	public void setSenderIBAN(String senderIBAN) {
		ValidationUtil.ibanValidation(senderIBAN);
		
		this.senderIBAN = senderIBAN;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		ValidationUtil.greaterThanZero(amount);
		
		this.amount = amount;
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

}
