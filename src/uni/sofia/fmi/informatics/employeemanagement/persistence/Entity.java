package uni.sofia.fmi.informatics.employeemanagement.persistence;

/**
 * Base class for all entities, POJO represents a db table 
 * for the application 
 * 
 * @author simo
 *
 */
public abstract class Entity {

	private static long counter = 0;
	
	private long id;

	public Entity () {
		this.id = counter++;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
