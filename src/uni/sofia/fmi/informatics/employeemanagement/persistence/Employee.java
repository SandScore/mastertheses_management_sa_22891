package uni.sofia.fmi.informatics.employeemanagement.persistence;

import java.util.ArrayList;
import java.util.List;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.EmployeeExtender;
import uni.sofia.fmi.informatics.employeemanagement.util.ValidationUtil;

/**
 * Persistence entity represents an Employee in the company 
 * 
 * @author simo
 *
 */
public class Employee extends Entity {

	private String firstName;
	private String lastName;
	private double salary;
	private String iban;
	private BankType bank;
	private List<Employee> subordinates;
	
	private EmployeeExtender visitor;
	
	public Employee() {
		this.subordinates = new ArrayList<Employee>();
		this.visitor = new EmployeeExtender(this);
	}
	
	public Employee (String fn, String ln, String iban, BankType bank, double salary) {
		this.subordinates = new ArrayList<Employee>();
		this.visitor = new EmployeeExtender(this);

		this.setFirstName(fn);
		this.setLastName(ln);
		this.setIban(iban);
		this.setBank(bank);
		this.setSalary(salary);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
		this.visitor.notifyObservers();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
		this.visitor.notifyObservers();
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		ValidationUtil.greaterThanZero(salary);
		
		this.salary = salary;
		this.visitor.notifyObservers();
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		ValidationUtil.ibanValidation(iban);
		
		this.iban = iban;
		this.visitor.notifyObservers();
	}

	public BankType getBank() {
		return bank;
	}

	public void setBank(BankType bank) {
		this.bank = bank;
		this.visitor.notifyObservers();
	}

	public List<Employee> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(List<Employee> subordinates) {
		this.subordinates = subordinates;
		this.visitor.notifyObservers();
	}

	public EmployeeExtender getVisitor() {
		return visitor;
	}
	
}
