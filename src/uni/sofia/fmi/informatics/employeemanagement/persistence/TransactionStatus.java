package uni.sofia.fmi.informatics.employeemanagement.persistence;

public enum TransactionStatus {

	PENDING,
	
	ORDERED,
	
	SUCCESSFUL, 
	
	EXCEPTION;
	
}
