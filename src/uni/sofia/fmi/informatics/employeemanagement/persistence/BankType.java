package uni.sofia.fmi.informatics.employeemanagement.persistence;

public enum BankType {

	DSK,
	FIBANK
}
