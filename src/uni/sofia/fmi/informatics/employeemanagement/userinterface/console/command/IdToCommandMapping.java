package uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command;

import java.util.HashMap;
import java.util.Map;

import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.ConsoleClient;
import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.commands.AddEmployee;
import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.commands.PaySalary;
import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.commands.PrintEmployee;

public class IdToCommandMapping {
	
	public static Map<String, Command> idToCommand;
	
	static {
		idToCommand = new HashMap<String, Command>();
		
		idToCommand.put("add", new AddEmployee(ConsoleClient.employeeManagement));
		idToCommand.put("pay", new PaySalary(ConsoleClient.employeeManagement));
		idToCommand.put("print", new PrintEmployee(ConsoleClient.employeeManagement));
	}
	
//	
//	private IdToObserverMapping () { init(); }
//	
//	private static void init() {
//		idToObserverClazz = new HashMap<String, Class<? extends EmployeeViewObserver>>();
//		
//		idToObserverClazz.put(TableViewObserver.ID, TableViewObserver.class);
//		idToObserverClazz.put(ListViewObserver.ID, ListViewObserver.class);
//	}
//	
//	public static final Class<? extends EmployeeViewObserver> getClazzById(String ID) {
//		return idToObserverClazz.get(ID);
//	}

}
