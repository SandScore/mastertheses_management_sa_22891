package uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.commands;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.api.EmployeeManagement;
import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.Command;

public class PaySalary implements Command {

	private EmployeeManagement employeeManagement;
	
	public PaySalary(EmployeeManagement employeeManagement) {
		this.employeeManagement = employeeManagement;
	}
	
	@Override
	public void execute(String... args) {
		long employeeId = 0;
		
		if (args != null) {
			if (args.length > 0) {
				String employeeIdAgr = args[0];
				employeeId = Long.parseLong(employeeIdAgr);
			}
			this.employeeManagement.paySalary(employeeId);
		}
		
	}

}
