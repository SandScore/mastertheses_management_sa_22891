package uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command;

public interface Command {
	void execute(String ... args);
}
