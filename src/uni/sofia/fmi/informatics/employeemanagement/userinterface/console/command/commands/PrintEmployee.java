package uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.commands;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.api.EmployeeManagement;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer.EmployeeViewObserver;
import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.Command;

public class PrintEmployee implements Command {

	private EmployeeManagement employeeManagement;
	
	public PrintEmployee(EmployeeManagement employeeManagement) {
		this.employeeManagement = employeeManagement;
	}
	
	@Override
	public void execute(String... args) {
		long employeeId = 0;
		String viewId = null;
		
		if (args != null) {
			int len = args.length;
			
			if (len > 0) {
				String employeeIdArg = args[0];
				employeeId = Long.parseLong(employeeIdArg);
			}
			if (len > 1) {
				viewId = args[1];
			}
			
			EmployeeViewObserver observer = this.employeeManagement.obtainView(employeeId, viewId);
			
			if (observer != null) {
				observer.print(System.out);
			}
		}
	}

}
