package uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.commands;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.api.EmployeeManagement;
import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.Command;

public class PaySubordinates implements Command {

	private EmployeeManagement employeeManagement;
	
	public PaySubordinates(EmployeeManagement employeeManagement) {
		this.employeeManagement = employeeManagement;
	}
	
	@Override
	public void execute(String... args) {
		long employeeId = 0;
		BankType bank = null;
		
		if (args != null) {
			int len = args.length;
			
			if (len > 0) {
				String employeeIdArg = args[0];
				employeeId = Long.parseLong(employeeIdArg);
			}
			if (len > 1) {
				String bankArg = args[1];
				bank = BankType.valueOf(bankArg);
			}
			
			this.employeeManagement.payToSubordinatesByBank(bank, employeeId);
		}
	}

}
