package uni.sofia.fmi.informatics.employeemanagement.userinterface.console;

import java.util.Scanner;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.api.EmployeeManagement;
import uni.sofia.fmi.informatics.employeemanagement.difactory.Factory;
import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.Command;
import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.IdToCommandMapping;

public class ConsoleClient {

	public static EmployeeManagement employeeManagement;
	
	public static void main(String[] args) {
		employeeManagement = Factory.inMemoryAndSOAPcontext();
		
		System.out.println("Welcome. Write your command, please: ");
		
		System.out.println("add <firstName> <lastName> <IBAN> <bank> <salary> ");
		System.out.println("print <employeeId> <TableViewObserver | ListViewObserver> ");
		System.out.println("pay <employeeId>");
		
		while(true) {
			Scanner scanner = new Scanner(System.in);
			String commandLine = scanner.nextLine();
			
			if (commandLine != null && !commandLine.isEmpty()) {
				
				String[] arguments = commandLine.split(" ");
				
				Command command = IdToCommandMapping.idToCommand.get(arguments[0]);
				
				if (command != null) {
					String[] argumentsToPass = new String[arguments.length - 1];
					boolean flag = false;
					int index = 0;
					for (String arg : arguments) {
						if (flag) {
							argumentsToPass[index++] = arg;
						} else {
							flag = true;
						}
					}
					
					command.execute(argumentsToPass);
					
					System.out.print("command executed succesfully. Next one ? ");
				}
			} else {
				scanner.close();
				System.out.println("buy");
				System.exit(0);
			}
		}
	}
}
