package uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.commands;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.api.EmployeeManagement;
import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.userinterface.console.command.Command;

public class AddEmployee implements Command {

	private EmployeeManagement employeeManagement;
	
	public AddEmployee(EmployeeManagement employeeManagement) {
		this.employeeManagement = employeeManagement;
	}
	
	@Override
	public void execute(String... args) {
		String firstName = null;
		String lastName = null;
		String iban = null;
		BankType bank = null;
		double salary = 0;
		
		if (args != null) {
			int len = args.length;
			if (len > 0) {
				firstName = args[0];
			}
			if (len > 1) {
				lastName = args[1];
			}
			if (len > 2) {
				iban = args[2];
			}
			if (len > 3) {
				String bankArg = args[3];
				bank = BankType.valueOf(bankArg);
			}
			if (len > 4) {
				String salaryArg = args[4];
				salary = Double.parseDouble(salaryArg);
			}
	
			this.employeeManagement.addEmployee(firstName, lastName, salary, bank, iban);
		}
		
	}

}
