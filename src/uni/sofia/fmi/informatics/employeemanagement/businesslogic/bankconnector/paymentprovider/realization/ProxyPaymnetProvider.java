package uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.realization;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.PaymentProvider;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.PaymentStatus;

public class ProxyPaymnetProvider implements PaymentProvider {

	public SOAPPaymentProvider realPaymentProvider; 
	
	@Override
	public PaymentStatus requestTransaction(String receiverIBAN,
			String senderIBAN, double amount) {
		// perform real connection to the payment provider's server 
		
		return this.realPaymentProvider.requestTransaction(receiverIBAN, senderIBAN, amount);
	}
}