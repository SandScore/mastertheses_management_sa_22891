package uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.realization;

import java.util.Date;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.api.BankConnector;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.PaymentProvider;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.PaymentStatus;
import uni.sofia.fmi.informatics.employeemanagement.dao.api.TransactionDAO;
import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Transaction;
import uni.sofia.fmi.informatics.employeemanagement.persistence.TransactionStatus;

public class BankConnectorImpl implements BankConnector {

	private PaymentProvider paymentProvider;
	private TransactionDAO transactionDAO;
	
	public static final String COMPANY_IBAN = "123456780";
	
	public BankConnectorImpl(PaymentProvider paymentProvider, TransactionDAO transactionDAO) {
		this.paymentProvider = paymentProvider;
		this.transactionDAO = transactionDAO;
	}
	
	@Override
	public Transaction transferMoney(String receiverIBAN, double amount,
			BankType bank) {
		PaymentStatus status = paymentProvider.requestTransaction(receiverIBAN, COMPANY_IBAN, amount);
		
		Transaction transaction = null;
		if (PaymentStatus.SUCCESSFULL.equals(status)) {
		    transaction = this.transactionDAO.create(receiverIBAN, COMPANY_IBAN,
		    		TransactionStatus.SUCCESSFUL, amount, new Date());
		}
		
		return transaction;
	}

}
