package uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.realization;

import java.util.HashMap;
import java.util.Map;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.Deserializator;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.PaymentProvider;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.PaymentStatus;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.Serializattor;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.response.ResponseCommandHandler;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.realization.response.CanceledResponse;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.realization.response.DeclinedResponse;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.realization.response.SuccessfulResponse;

public class SOAPPaymentProvider implements PaymentProvider {

	private Serializattor serializator;
	private Deserializator deserializator;
	
	private static Map<Integer, ResponseCommandHandler> RESPONSE_IDENTIFICATOR_TO_RESPONSE_HANDLER = 
			new HashMap<Integer, ResponseCommandHandler>();
	
	static {
		RESPONSE_IDENTIFICATOR_TO_RESPONSE_HANDLER.put(Integer.valueOf(100), new SuccessfulResponse());
		RESPONSE_IDENTIFICATOR_TO_RESPONSE_HANDLER.put(Integer.valueOf(101), new DeclinedResponse());
		RESPONSE_IDENTIFICATOR_TO_RESPONSE_HANDLER.put(Integer.valueOf(102), new CanceledResponse());
	}
	
	public SOAPPaymentProvider() {
		this.serializator = new SOAPSerializator();
		this.deserializator = new SOAPDeserializator();
	}
	
	@Override
	public PaymentStatus requestTransaction(String receiverIBAN,
			String senderIBAN, double amount) {
		
		
		return PaymentStatus.SUCCESSFULL;
	}

}
