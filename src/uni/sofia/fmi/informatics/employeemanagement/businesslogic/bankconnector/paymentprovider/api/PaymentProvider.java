package uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api;

/**
 * A contract for all implementations of a payment provider going to be integrated in the system 
 * Implementations means in a web service type they are connected and collaborate with the external world 
 * 
 * So far, they are :
 * <ul>
 * 		<li>SOAP base</li>
 * 		<li>HTTPS Name Value Pair (NVP) POST </li>
 * </ul>
 * 
 * The are some other strategies for integration, but at least they are approved as one of the most 
 * secure ways. 
 * 
 * For some of the payment provider integration an auto generated stub could be required.
 * Like in the case for SOAP base, where the WSDL delivered by the payment provider is used in 
 * order the java source code to be generated 
 * 
 * 
 * @author simo
 *
 */
public interface PaymentProvider {

	/**
	 * Perform a transaction for amount of money : <code>amount</amount> from bank account 
	 * <code>senderIBAN</code> to bank account <code>receiverIBAN</code> 
	 * 
	 * @param receiverIBAN
	 * 		the IBAN number of the bank account where the money will be transfered 
	 * 
	 * @param senderIBAN
	 * 		the IBAN of the bank account from where the money will be taken 
	 * 
	 * @param amount
	 * 		the amount of money which are going to be used in the transaction operation 
	 * 
	 * @return
	 * 		a PaymentStatus indicates the end transaction status of the payment 
	 * 
	 */
	PaymentStatus requestTransaction(String receiverIBAN, String senderIBAN, double amount);
	
}
