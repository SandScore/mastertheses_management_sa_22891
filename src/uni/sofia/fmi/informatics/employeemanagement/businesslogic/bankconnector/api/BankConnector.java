package uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.api;

import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Transaction;

/**
 * A contract identifies all available services for bank transaction related operations.
 *  
 * Takes care about the business logic to connect to a concrete bank, to execute a payment 
 * / transaction service with specific web service implementation : SOAP, NVP, REST and etc. 
 * depends on the service delivered by the concrete bank
 * 
 * @author simo
 *
 */
public interface BankConnector {

	/**
	 * Transfer <code>money</code> from the company's bank account to the 
	 * IBAN identified with <code>receiverIBAN</code> using bank connection indicated with 
	 * <code>bank</code> argument 
	 * 
	 * @param receiverIBAN
	 * 		IBAN, where the amount of money will be transfered
	 * @param amount
	 * 		the amount of going to be transfered 
	 * @param bankType
	 * 		the BankType indicates the bank going to be used 
	 * 
	 * @return
	 * 		an instance of Transaction indicating the result of the money transaction 
	 */
	Transaction transferMoney(String receiverIBAN, double amount, BankType bank);
}
