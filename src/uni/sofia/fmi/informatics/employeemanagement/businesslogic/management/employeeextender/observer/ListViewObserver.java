package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer;

import java.io.PrintStream;

/**
 * Extender of EmployeeViewObserver in order to deliver a required information in
 * a List view 
 * 
 * @author simo
 *
 */
public class ListViewObserver extends EmployeeViewObserver {

	public static final String ID = TableViewObserver.class.getSimpleName();
	
	
	@Override
	public void print(PrintStream stream) {
		assert (stream != null);
		
		if (this.abesrvable != null) {
			stream.println("name: " + this.abesrvable.getFirstName() + " " + this.abesrvable.getLastName() );
			stream.println("salary: " + this.abesrvable.getSalary());
			stream.println("count subordinates: " + this.abesrvable.getSubordinates().size());
			stream.println("bank: " + this.abesrvable.getBank().toString());
			stream.println("iban: " + this.abesrvable.getIban());
		}
		
	}
	
	public String getId() {
		return ID;
	}
	
}
