package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer;

import java.io.PrintStream;

import uni.sofia.fmi.informatics.employeemanagement.persistence.Employee;

/**
 * Extender of Observer in order to deliver required contract for view of 
 * an employee - as observer 
 * 
 * @author simo
 * 
 */
public abstract class EmployeeViewObserver implements Observer<Employee> {
	
	protected Employee abesrvable; 
	
	public abstract void print(PrintStream stream);
	
	public void update(Employee abservable) {
		this.abesrvable = abservable;
	}
	
	abstract String getId();
}
