package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer;

import java.io.PrintStream;

/**
 * Extender of EmployeeViewObserver in order to deliver a required information in
 * a List view 
 * 
 * @author simo
 * 
 */
public class TableViewObserver extends EmployeeViewObserver {

	public static final String ID = TableViewObserver.class.getSimpleName();
	
	@Override
	public void print(PrintStream stream) {
		assert (stream != null);
		
		stream.println("|---------------------------------------------------|");
		stream.println("|First Name | Last Name | Salary | Bank   | IBAN    |");
		stream.println("|---------------------------------------------------|");
		stream.println("|"+ this.abesrvable.getFirstName() + "|"
				+ this.abesrvable.getLastName() + "|"
						+ this.abesrvable.getSalary() + "|"
								+ this.abesrvable.getBank() + "|"
										+ this.abesrvable.getIban() +"|");
		stream.println("|---------------------------------------------------|");
	}	
	
	public String getId() {
		return ID;
	}
}
