package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.filter;

import java.util.Iterator;

import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Employee;

/**
 * It's a filter base of defining of an Employee belongs to relevant bank 
 * 
 * @author simo
 *
 */
public class BankFilter implements FilterIterator<Employee> {

	private BankType bankType;
	private Iterator<Employee> subordinates;
	private Employee next;
	
	public BankFilter(Employee employee, BankType bankType) {
		this.subordinates = employee.getSubordinates().iterator();
		this.bankType = bankType;
		this.accessNext();
	}
	
	@Override
	public Employee next() {
		if (this.next == null) {
			this.accessNext();
		}
		
		return this.next;
	}

	@Override
	public boolean hasNext() {
		return (this.next != null);
	}
	
	private void accessNext() {
		Employee temp = null;
		while (this.subordinates.hasNext()) {
			temp = this.subordinates.next();
			if (temp != null && (this.bankType.equals(temp.getBank()))) {
				this.next = temp;
				break;
			}
		}
	}

}
