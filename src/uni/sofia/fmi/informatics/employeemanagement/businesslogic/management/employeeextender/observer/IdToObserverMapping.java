package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer;

import java.util.HashMap;
import java.util.Map;

public final class IdToObserverMapping {

	private static Map<String, Class<? extends EmployeeViewObserver>> idToObserverClazz;
	
	private IdToObserverMapping () {}
	
	static {
		idToObserverClazz = new HashMap<String, Class<? extends EmployeeViewObserver>>();
		
		idToObserverClazz.put(TableViewObserver.ID, TableViewObserver.class);
		idToObserverClazz.put(ListViewObserver.ID, ListViewObserver.class);
	}
	
	public static final Class<? extends EmployeeViewObserver> getClazzById(String ID) {
		return idToObserverClazz.get(ID);
	}
	
}
