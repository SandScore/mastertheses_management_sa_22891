package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.filter;

/**
 * Arises a contract for a filter iterator  
 * 
 * @author simo
 *
 * @param <T>
 */
public interface FilterIterator<T> {

	T next();
	boolean hasNext();
}
