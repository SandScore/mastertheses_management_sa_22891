package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.realization;

import java.util.ArrayList;
import java.util.List;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.api.BankConnector;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.api.EmployeeManagement;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.filter.FilterIterator;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer.EmployeeViewObserver;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer.IdToObserverMapping;
import uni.sofia.fmi.informatics.employeemanagement.dao.api.EmployeeDAO;
import uni.sofia.fmi.informatics.employeemanagement.dao.api.TransactionDAO;
import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Employee;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Transaction;

public class EmployeeManagementImpl implements EmployeeManagement {

	private EmployeeDAO employeDAO;
	private TransactionDAO transactionDAO;
	private BankConnector bankConnector;
	
	public EmployeeManagementImpl(
			EmployeeDAO employeeDAO, TransactionDAO transactionDAO, BankConnector bankConnector) {
		this.employeDAO = employeeDAO;
		this.transactionDAO = transactionDAO;
		this.bankConnector = bankConnector;
	}
	
	@Override
	public Transaction paySalary(long employeeId) {
		Employee employee = this.employeDAO.findById(employeeId, Employee.class);
		
		Transaction salaryPayment = null;
		if (employee != null) {
			salaryPayment = this.bankConnector.transferMoney(
					employee.getIban(), employee.getSalary(), employee.getBank());
		}
		
		return salaryPayment;
	}

	@Override
	public List<Transaction> payToSubordinatesByBank(BankType type,
			long employeeId) {
		Employee employee = this.employeDAO.findById(employeeId, Employee.class);
		FilterIterator<Employee> iterator = employee.getVisitor().getBankFilter(type);
		
		List<Transaction> transactions = new ArrayList<Transaction>();
		while (iterator.hasNext()) {
			Employee toPayEmployee = iterator.next();
			Transaction transaction = 
					this.bankConnector.transferMoney(
					 toPayEmployee.getIban(), toPayEmployee.getSalary(), toPayEmployee.getBank());
			 
			 transactions.add(transaction);
		}
		
		return transactions;
	}

	@Override
	public Employee addEmployee(String firstName, String lastName,
			double salary, BankType bank, String iban) {
		return employeDAO.create(firstName, lastName, salary, iban, bank);
	}

	@Override
	public Employee getEmployeeById(long emploiyeeId) {
		return employeDAO.findById(emploiyeeId, Employee.class);
	}

	@Override
	public Employee removeEmployee(long employeeId) {
		Employee employee = getEmployeeById(employeeId);
		
		this.employeDAO.remove(employee);
		
		return employee;
	}

	@Override
	public boolean updateEmployee(Employee employee) {
		this.employeDAO.save(employee);
		
		return true;
	}

	@Override
	public EmployeeViewObserver obtainView(long employeeId, String viewId) {
		Employee employee = getEmployeeById(employeeId);
		
		Class<? extends EmployeeViewObserver> observerClazz = IdToObserverMapping.getClazzById(viewId);
		EmployeeViewObserver observer = employee.getVisitor().getViewByType(observerClazz);
		
		return observer;
	}

}
