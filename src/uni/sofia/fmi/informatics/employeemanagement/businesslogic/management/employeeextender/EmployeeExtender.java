package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender;

import java.util.HashMap;
import java.util.Map;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.filter.BankFilter;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.filter.FilterIterator;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer.EmployeeViewObserver;
import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Employee;

/**
 * Business class representation of Employee entity. 
 * 
 * Delivers specific business related functionality for wrapped Employee
 * 
 * Will play the role of Visitor to the employee !
 * 
 * @author simo
 *
 */
public class EmployeeExtender {
	
	private Employee employee;
	
	private Map<Class<? extends EmployeeViewObserver>, EmployeeViewObserver> viewObservers;
	
	public EmployeeExtender(Employee employee) {
		this.employee = employee;
		this.viewObservers  = new HashMap<Class<? extends EmployeeViewObserver>, EmployeeViewObserver>();
	}
	
	public void registerObserver(EmployeeViewObserver observer) {
		if (observer != null) {
			this.viewObservers.put(observer.getClass(), observer);
		}
	}
	
	public void unregisterObserver(EmployeeViewObserver observer) {
	    this.viewObservers.remove(observer.getClass());
	}
	
	public void notifyObservers() {
		for (EmployeeViewObserver observer : this.viewObservers.values()) {
			observer.update(employee);
		}
	}
	
	// ------ get extra functionality added in modules for the employee -------
	
	public EmployeeViewObserver getViewByType(Class<? extends EmployeeViewObserver> type) {
		return this.viewObservers.get(type);
	}

	public FilterIterator<Employee> getBankFilter(BankType type) {
		return new BankFilter(this.employee, type);
	}
}
