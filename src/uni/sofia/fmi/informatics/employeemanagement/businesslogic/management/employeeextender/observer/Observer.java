package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer;

/**
 * Arise an interface, a contract, for each kind of observer - a class
 * should observe another class in order to deliver additional, extra functionality 
 * to it. 
 * 
 * @author simo
 *
 * @param <T>
 */
public interface Observer<T> {
	
	void update(T abervable);
}
