package uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.api;

import java.util.List;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.employeeextender.observer.EmployeeViewObserver;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Employee;
import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Transaction;

/**
 * A contract that should describe all available services for employee related operations.
 *  
 * It's a main behavioral point delivered to the clients in order to execute commands 
 * in the context of the application. 
 * 
 * The business layer of the application is realized vis usage of a simple dependency injection 
 * pattern, which in some way intercepts the requests and delegates them to the appropriate 
 * implementations of the related dependencies 
 * 
 * @see Factory 
 * 
 * @author simo
 *
 */
public interface EmployeeManagement {

	// payment transactions related operations
	
	/**
	 * Pay monthly salary for employee with passed ID <code>employeeId</code>
	 * 
	 * PRE-CONDITIONS: 
	 * 		the Employee with identified id should have set up salary and IBAN number 
	 * 
	 * @param employeeId
	 * 		the id of the Employee monthly salary is going to be passed
	 * 
=	 * @return
	 * 		an instance of Transaction with information about the paying 
	 */
	Transaction paySalary(long employeeId);
		
	/**
	 * Execute payment transaction to all sub-ordinates belongs to bank with type
	 * <code>type</code> of employee with passed id <code>employeeId</code>  
	 * 
	 * @param type
	 * 		BankType indicates to which bank the money will be transfered 
	 * 
	 * @param employeeId
	 * 		the Employee id which sub-ordinates money transfered will be done 
	 * 
	 * @return
	 * 		a list of Transaction entities shows what the result of each transaction is 
	 */
	List<Transaction> payToSubordinatesByBank(BankType type, long employeeId);
	
	// CRUD operations related to Employee
	
	/**
	 * Add a new Employee to the system with the initial state related to him/her. 
	 * 
	 * PRE-CONDITIONS: 
	 * 		<ul>	
	 * 			<li><code>iban</iban>, <code>salary</code> and bank type should be not NULL pointer</li>
	 * 			<li><code>iban</iban> should be unique - not registered in the system yet
	 * 		</ul>
	 * 
	 * 
	 * @param firstName
	 * 		first name of the employee 
	 * @param lastName
	 * 		last name of the employee 
	 * @param salary
	 * 		monthly salary of the employee 
	 * @param bank
	 * 		the bank institution where the employee is registered 
	 * @param iban
	 * 		IBAN number of the employee where the salary is going to be payed 
	 * 
	 * @return
	 * 		new create and persisted instance of Employee with the data passed as arguments
	 */
	Employee addEmployee(String firstName, String lastName, double salary, BankType bank, String iban);
	
	/**
	 * Get an Employee from the data base identified with passed ID <code>emploiyeeId</code>
	 *  
	 * @param emploiyeeId
	 * 		The {@link Long} id of the Employee, whose information is needed.
	 * @return
	 * 		The Employee with the given id or null if it was not found.
	 */
	Employee getEmployeeById(long emploiyeeId);
	
	/**
	 * Remove from the database an employee with passed id <code>employeeId</code>
	 * 
	 * @param employeeId
	 *     The {@link Long} id of the Employee, who is going ot be removed
	 * 
	 * @return
	 * 	   removed instance of Employee 
	 */
	Employee removeEmployee(long employeeId);
	
	/**
	 * Update information for passed employee into the data base as well 
	 * 
	 * @param employee
	 * 		Employee being updated 
	 * @return
	 * 		true only in case update operation passed successful
	 * 
	 */
	boolean updateEmployee(Employee employee);
	
	// view related operations -------------------------------------------
	
	/**
	 * Obtain required view for an Employee unequally identified with passed ID 
	 * 
	 * @param employeeId
	 * 		the employee ID which view is looking for
	 * 
	 * @param viewId
	 * 		view ID - an implementation of EmployeeViewObserver
	 * 
	 * @return
	 * 		requested view implementation of EmployeeViewObserver
	 */
	EmployeeViewObserver obtainView(long employeeId, String viewId);
	
}
