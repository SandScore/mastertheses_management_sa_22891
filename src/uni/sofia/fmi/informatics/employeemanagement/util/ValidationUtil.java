package uni.sofia.fmi.informatics.employeemanagement.util;

import uni.sofia.fmi.informatics.employeemanagement.exception.ErrorCode;
import uni.sofia.fmi.informatics.employeemanagement.exception.BusinessException;

public final class ValidationUtil {

	private ValidationUtil() {}
	
	public static final void ibanValidation(String iban) {
		if (iban == null) {
			throw new BusinessException(ErrorCode.INCORRECT_BANK_ACCOUNT);
		}
	}
	
	public static final void greaterThanZero(Number amount) {
		if (amount == null || amount.doubleValue() < 0) {
			throw new BusinessException(ErrorCode.POSSITIVE_AMOUNT_ERROR);
		}
	}
	
}
