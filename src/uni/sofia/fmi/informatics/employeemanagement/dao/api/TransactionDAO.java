package uni.sofia.fmi.informatics.employeemanagement.dao.api;

import java.util.Date;

import uni.sofia.fmi.informatics.employeemanagement.persistence.Transaction;
import uni.sofia.fmi.informatics.employeemanagement.persistence.TransactionStatus;

/**
 * A contract for a Data Access Object for CRUD operations related to a Transaction entity 
 * 
 * Implementors of this contract should be separated in meaning of the data base context the application 
 * works: desk base, like NoSQL server base or relational like MySQL implementation, or in-memory one
 * 
 * 
 * @author simo
 *
 */
public interface TransactionDAO extends GenericDAO<Transaction> {

	/**
	 * Create and persist a new Transaction into the data base used with used data 
	 * state passed via method arguments.
	 * 
	 * @param receiverIBAN
	 * 		IBAN number of the bank account of the receiver - payee
	 * 
	 * @param senderIBAN
	 * 		IBAN number of the bank account of the sender - payer
	 * 
	 * @param status
	 * 		a TransacionStatus indicates how the transaction passed
	 * 		
	 * @param amount
	 * 		amount of this transaction 
	 * 
	 * @param date
	 * 		the Date when the transaction has been executed 
	 * 
	 * @return
	 * 		new created and persisted instance of Transaction 
	 */
	Transaction create(
			String receiverIBAN, String senderIBAN,TransactionStatus status, double amount, Date date);

}
