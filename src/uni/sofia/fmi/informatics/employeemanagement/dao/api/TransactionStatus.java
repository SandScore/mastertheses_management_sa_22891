package uni.sofia.fmi.informatics.employeemanagement.dao.api;

public enum TransactionStatus {

	PENDING, 
	ORDERED,
	SUCCESSFUL,
	EXCEPTION;
}
