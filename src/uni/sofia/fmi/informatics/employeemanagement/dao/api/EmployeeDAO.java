package uni.sofia.fmi.informatics.employeemanagement.dao.api;

import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Employee;

/**
 * Extension of Data Access layering for CRUD operation related to Employee 
 * 
 * @author simo
 *
 */
public interface EmployeeDAO extends GenericDAO<Employee> {

	/**
	 * Create and persist a new employee with passed value fields 
	 * 
	 * @param firstName
	 * 		employee's first name 
	 * 
	 * @param lastName
	 * 		employee's last name 
	 * 
	 * @param salary
	 * 		employee's  salary 
	 * 
	 * @param iban
	 * 		employee's IBAN
	 * 
	 * @param bank
	 * 		employee's bank used
	 * 
	 * @return
	 * 		new created and persisted employee 
	 */
	 Employee create(String firstName, String lastName, double salary, String iban, BankType bank);
}
