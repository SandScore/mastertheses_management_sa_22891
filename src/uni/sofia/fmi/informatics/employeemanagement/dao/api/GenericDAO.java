package uni.sofia.fmi.informatics.employeemanagement.dao.api;

import java.util.List;

import uni.sofia.fmi.informatics.employeemanagement.persistence.Entity;

/**
 * represents generic, available for all kind extensions, Data Access Objects 
 * for Data Access Layer. 
 * 
 * @author simo
 *
 * @param <T>
 */
public interface GenericDAO<T extends Entity> {
	
	/**
	 * Find an Entity by id
	 * 
	 * @param Id
	 * 		the ID of the entity we are searching for 
	 * 
	 * @return
	 * 		the entity associated with passed ID
	 */
	T findById(long Id, Class<T> clazz);

	/**
	 * Search and return a set of entities in a range indicated by 
	 * their id's. The max results can be <code>maxreults</code> 
	 * starting from position <code>startPosition</code>
	 * 
	 * @param startPosition
	 * 		the position from where the search is starting 
	 * 
	 * @param maxReults
	 * 		max results of the entities we re looking for  
	 * 
	 * @return
	 * 		a set of entities we are searching for 
	 */
	List<T> findInRange(long startPosition, long maxReults,  Class<T> clazz);
	
	/**
	 * Persist passed entity into the database 
	 * 
	 * @param entity
	 * 		entity we are going to persist into the related database 
	 * 
	 */
	void save(T entity);
	
	/**
	 * Remove passed entity from the database associated 
	 * 
	 * @param entity
	 * 		entity we are going to removed from the DB
	 */
	void remove(T entity);
}
