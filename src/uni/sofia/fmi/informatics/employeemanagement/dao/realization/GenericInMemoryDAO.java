package uni.sofia.fmi.informatics.employeemanagement.dao.realization;

import java.util.List;

import uni.sofia.fmi.informatics.employeemanagement.dao.api.GenericDAO;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Entity;
import uni.sofia.fmi.informatics.employeemanagement.persistence.inmemorydb.InMemoryDB;

/**
 * generic implementation of in MEMORY DATA BASE ACCESS 
 * 
 * @author simo
 *
 * @param <T>
 */
public class GenericInMemoryDAO<T extends Entity> implements GenericDAO<T> {

	protected InMemoryDB db;
	
	public GenericInMemoryDAO(InMemoryDB db) {
		this.db = db;
	}
	
	@Override
	public T findById(long Id,  Class<T> clazz) {
		return this.db.findById(clazz, Id);
	}

	@Override
	public List<T> findInRange(long startPosition, long maxReults,  Class<T> clazz) {
		
		return null;
	}

	@Override
	public void save(T entity) {
		this.db.add(entity);
	}

	@Override
	public void remove(T entity) {
		this.db.remove(entity);
	}

}
