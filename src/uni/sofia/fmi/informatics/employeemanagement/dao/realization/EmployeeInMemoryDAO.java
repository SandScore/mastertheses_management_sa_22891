package uni.sofia.fmi.informatics.employeemanagement.dao.realization;

import uni.sofia.fmi.informatics.employeemanagement.dao.api.EmployeeDAO;
import uni.sofia.fmi.informatics.employeemanagement.persistence.BankType;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Employee;
import uni.sofia.fmi.informatics.employeemanagement.persistence.inmemorydb.InMemoryDB;


public class EmployeeInMemoryDAO extends GenericInMemoryDAO<Employee> implements EmployeeDAO {

	public static int GLOBAL_COUNTER = 1;
	
	public EmployeeInMemoryDAO(InMemoryDB db) {
		super(db);
	}
	
	public Employee create(String firstName, String lastName, 
			double salary, String iban, BankType bank) {
		
		Employee employee = new Employee(firstName, lastName, iban, bank, salary);
		employee.setId(GLOBAL_COUNTER++);
		this.save(employee);
		
		return employee;
	}
}
