package uni.sofia.fmi.informatics.employeemanagement.dao.realization;

import java.util.Date;

import uni.sofia.fmi.informatics.employeemanagement.dao.api.TransactionDAO;
import uni.sofia.fmi.informatics.employeemanagement.persistence.Transaction;
import uni.sofia.fmi.informatics.employeemanagement.persistence.TransactionStatus;
import uni.sofia.fmi.informatics.employeemanagement.persistence.inmemorydb.InMemoryDB;

public class TransactionInMemoryDAO extends GenericInMemoryDAO<Transaction> implements
		TransactionDAO {

	public TransactionInMemoryDAO(InMemoryDB db) {
		super(db);
	}

	@Override
	public Transaction create(String receiverIBAN, String senderIBAN,
			TransactionStatus status, double amount, Date date) {
		
		Transaction transaction = 
				new Transaction(
				receiverIBAN, senderIBAN, date, amount);
		transaction.setStatus(status);
		
		return transaction;
	}

}
