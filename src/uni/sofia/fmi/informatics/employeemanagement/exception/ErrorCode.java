package uni.sofia.fmi.informatics.employeemanagement.exception;

public enum ErrorCode {

	INCORRECT_BANK_ACCOUNT(1, "IBAN not valid"),
	AMOUNT_NOT_IN_RANGE(2, "entered amount not in range"),
	POSSITIVE_AMOUNT_ERROR(3, "amount should be possitive number");
	
	private int code;
	private String message;
	
	ErrorCode(int code, String message) {
		
	}
	

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}
