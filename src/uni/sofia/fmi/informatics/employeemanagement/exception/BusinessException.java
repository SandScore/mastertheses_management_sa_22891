package uni.sofia.fmi.informatics.employeemanagement.exception;

public class BusinessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BusinessException(ErrorCode error) {
		super(error.getMessage());
	}
}
