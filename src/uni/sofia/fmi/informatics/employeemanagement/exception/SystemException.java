package uni.sofia.fmi.informatics.employeemanagement.exception;

public class SystemException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SystemException(ErrorCode error) {
		super(error.getMessage());
	}

}
