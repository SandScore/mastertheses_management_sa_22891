package uni.sofia.fmi.informatics.employeemanagement.difactory;

import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.api.BankConnector;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.api.PaymentProvider;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.realization.ProxyPaymnetProvider;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.paymentprovider.realization.SOAPPaymentProvider;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.bankconnector.realization.BankConnectorImpl;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.api.EmployeeManagement;
import uni.sofia.fmi.informatics.employeemanagement.businesslogic.management.realization.EmployeeManagementImpl;
import uni.sofia.fmi.informatics.employeemanagement.dao.api.EmployeeDAO;
import uni.sofia.fmi.informatics.employeemanagement.dao.api.TransactionDAO;
import uni.sofia.fmi.informatics.employeemanagement.dao.realization.EmployeeInMemoryDAO;
import uni.sofia.fmi.informatics.employeemanagement.dao.realization.TransactionInMemoryDAO;
import uni.sofia.fmi.informatics.employeemanagement.persistence.inmemorydb.InMemoryDB;

public final class Factory {
	
	private Factory () {}
	
	public static final EmployeeManagement inMemoryAndSOAPcontext () {
		EmployeeManagement employeeManagement = null;
		
		InMemoryDB db = new InMemoryDB();
		PaymentProvider paymentProvider = new ProxyPaymnetProvider();
		TransactionDAO transactionDAO = new TransactionInMemoryDAO(db);
		BankConnector bankConnector = new BankConnectorImpl(paymentProvider, transactionDAO);
		
		EmployeeDAO employeeDAO = new EmployeeInMemoryDAO(db);
		employeeManagement = new EmployeeManagementImpl(employeeDAO, transactionDAO, bankConnector); 
		
		return employeeManagement;
	}

	
}
